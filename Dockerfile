
FROM r-base:3.6.0

RUN apt-get update \
    apt-get install -y libcurl4-openssl-dev \
    R --slave -e 'install.packages(c("BiocManager","docopt"))' \
    R --slave -e 'BiocManager::install(c("dada2","DECIPHER","phangorn"))'

